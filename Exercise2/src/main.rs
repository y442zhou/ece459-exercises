// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    if n == 0 {
        return 0;
    } else if n == 1 || n == 2 {
        return 1;
    }

    let mut result: u32 = 2;
    let mut prev: u32 = 1;

    for _i in 3..n {
        let temp = prev;
        prev = result;
        result += temp;
    }

    result
}

fn main() {
    println!("{}", fibonacci_number(10));
}
