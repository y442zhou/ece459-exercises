use std::sync::mpsc;
use std::thread;
use std::time::Duration;

static N: i32 = 10;
// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();
    let mut children = vec![];
    for i in 0..N {
        let tx_clone = mpsc::Sender::clone(&tx);
        let handle = thread::spawn(move || {
            tx_clone.send(format!("Hello from thread {}.", i)).unwrap();
            thread::sleep(Duration::from_secs(1));
        });
        children.push(handle)
    }

    for child in children {
        child.join().unwrap();
    }

    let mut received_count = 0;
    for received in rx {
        println!("Got: {}", received);
        received_count += 1;
        if received_count == 10 {
            break;
        }
    }

    println!("Main thread terminated.");
}
